package id.co.asyst.amala.response;

import com.google.gson.Gson;
import id.co.asyst.commons.core.utils.DateUtils;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.text.SimpleDateFormat;
import java.util.*;

public class response {

    private static Logger logger = LogManager.getLogger(response.class);

    public void resBuild(Exchange exchange) {
        Message in = exchange.getIn();
        Message out = exchange.getOut();
        Gson gson = new Gson();

        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date d = new Date();
        Date createddate;
        createddate = d;
        exchange.setProperty("dateend", sdf2.format(createddate));

        Map<String, Object> identity = exchange.getProperty("identity", Map.class);
        Map<String, Object> getServiceResponse = new LinkedHashMap<>();
        Map<String, Object> status = exchange.getProperty("status", Map.class);
        Map<String, Object> statuscustom = new HashMap<>();
        Map<String, Object> result = exchange.getProperty("data", Map.class);
        Integer revise = exchange.getProperty("revise", Integer.class);
        Integer rejected = exchange.getProperty("rejected", Integer.class);
//        String reqhistoryid = GenerateId("RH", DateUtils.now(), 2);
        Date approvaldate = DateUtils.now();
        String createdby = identity.get("userid").toString();
//        exchange.setProperty("reqhistoryid", reqhistoryid);
        exchange.setProperty("approvaldate", approvaldate);
        exchange.setProperty("createdby", createdby);

        statuscustom.put("responsecode", "0000");
        statuscustom.put("responsedesc", "Success");
        statuscustom.put("responsemessage", "Data has been " + exchange.getProperty("reqstatus").toString().toLowerCase() + " successfully");
//        if(exchange.getProperty("reqstatus") == "REJECTED" || exchange.getProperty("reqstatus") == "REVISE"){
//            statuscustom.put("responsemessage", "Approval has been " + exchange.getProperty("reqstatus").toString().toLowerCase() + " successfully");
//        }

        result.put("approvalby", exchange.getProperty("approvalby"));
        result.put("responsedata", exchange.getProperty("responsedata"));

        if (status == null){
            getServiceResponse.put("status", statuscustom);
        } else {
            getServiceResponse.put("status", status);
        }

        getServiceResponse.put("identity", identity);
        getServiceResponse.put("result", result);
        String response = gson.toJson(getServiceResponse);
        out.setBody(response);
        logger.info("RESPONSE :::" + response);
        exchange.setProperty("resrjct", response);
    }

    public void setResponsedata(Exchange exchange) {
        Message in = exchange.getIn();
        Message out = exchange.getOut();
        Gson gson = new Gson();

        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date d = new Date();
        Date createddate;
        createddate = d;
        exchange.setProperty("dateend", sdf2.format(createddate));

        Map<String, Object> identity = exchange.getProperty("identity", Map.class);
        Map<String, Object> getServiceResponse = new LinkedHashMap<>();
        Map<String, Object> statuscustom = new HashMap<>();
        Map<String, Object> result = exchange.getProperty("reqdataDB", Map.class);
        Date approvaldate = DateUtils.now();
        String createdby = identity.get("userid").toString();
        exchange.setProperty("approvaldate", approvaldate);
        exchange.setProperty("createdby", createdby);

        statuscustom.put("responsecode", "0000");
        statuscustom.put("responsedesc", "Success");
        statuscustom.put("responsemessage", "Data has been " + exchange.getProperty("reqstatus").toString().toLowerCase() + " successfully");
//        if(exchange.getProperty("reqstatus") == "REJECTED" || exchange.getProperty("reqstatus") == "REVISE"){
//            statuscustom.put("responsemessage", "Approval has been " + exchange.getProperty("reqstatus").toString().toLowerCase() + " successfully");
//        }

        result.put("approvalby", exchange.getProperty("approvalby"));
//        result.put("responsedata", exchange.getProperty("responsedata"));

        getServiceResponse.put("status", statuscustom);
        getServiceResponse.put("identity", identity);
        getServiceResponse.put("result", result);
        String response = gson.toJson(getServiceResponse);
//        logger.info("RESPONSE :::" + response);
        exchange.setProperty("resdata", response);
    }

    public void errorResponse(Exchange exchange) {
        Message in = exchange.getIn();
        Message out = exchange.getOut();
        Exception except = exchange.getException();
        String body = in.getBody(String.class);
        Integer httpResponseCode = in.getHeader(exchange.HTTP_RESPONSE_CODE, Integer.class);
        Gson gson = new Gson();

        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date d = new Date();
        Date createddate;
        createddate = d;
        exchange.setProperty("dateend", sdf2.format(createddate));

        Map<String, Object> identity = exchange.getProperty("identity", Map.class);
        if (identity.containsKey("password")) {
            identity.replace("password",
                    identity.get("password").toString().replaceAll("[A-Za-z0-9]", "*").toString());
        }

        Map<String, Object> getServiceResponse = new HashMap<String, Object>();
        Map<String, Object> status = new HashMap<String, Object>();
        Date date = new Date();
        identity.replace("reqdate", sdf2.format(date));
        status.put("responsecode", "9999");
        status.put("responsedesc", "error");
        status.put("responsemessage", in.getHeader("error", String.class));

        getServiceResponse.put("identity", identity);
        getServiceResponse.put("status", status);
        String result = gson.toJson(getServiceResponse);
//        logger.info("HttpResponCode" + httpResponseCode);
//        logger.info("Responsemessage" + in.getHeader("error", String.class));
//        logger.info("result error" + result);
        out.setBody(result);
        exchange.setProperty("resrjct", result);
    }

    public void responseInvalid(Exchange exchange) {
        Message in = exchange.getIn();
        Message out = exchange.getOut();
        Gson gson = new Gson();
        Map<String, Object> identity = exchange.getProperty("identity", Map.class);
        Map<String, Object> response = new HashMap<String, Object>();
        Map<String, Object> status = new HashMap<String, Object>();

        status.put("responsecode", exchange.getProperty("rescode"));
        status.put("responsedesc", exchange.getProperty("resdesc"));
        status.put("responsemessage", exchange.getProperty("resmsg"));

        response.put("identity", identity);
        response.put("status", status);
        String result = gson.toJson(response);
        out.setBody(result);
    }

    public void responseSuccess(Exchange exchange) {
        Message in = exchange.getIn();
        Message out = exchange.getOut();
        Gson gson = new Gson();
        Map<String, Object> identity = exchange.getProperty("identity", Map.class);
        Map<String, Object> body = in.getBody(Map.class);
        logger.info("body response " +body);
        Map<String, Object> response = new HashMap<String, Object>();
        Map<String, Object> status = new HashMap<String, Object>();


        status.put("responsecode", "0000");
        status.put("responsedesc", "Success");
        status.put("responsemessage", "Data " + exchange.getProperty("keterangan") + " has been listed!");

        response.put("identity", identity);
        response.put("status", status);
        response.put("result", exchange.getProperty("resultfix"));
        String result = gson.toJson(response);
        out.setBody(result);
    }

    public void invalidBuild(Exchange exchange) {
        Message in = exchange.getIn();
        Message out = exchange.getOut();
        String body = in.getBody(String.class);
        Gson gson = new Gson();
        Map<String, Object> status = new HashMap<String, Object>();
        status.put("responsecode", "9005");
        status.put("responsedesc", "Invalid Data");
        status.put("responsemessage", "Data already approved");
        exchange.setProperty("status", status);
    }

    public static String GenerateId(String prefix, Date date, int numdigit) {


        SimpleDateFormat sdf = new SimpleDateFormat("yyMMdd");

        Random rand = new Random();

        String dateFormat = "";

        if (date != null)
            dateFormat = sdf.format(date);

        String[] arr = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f", "g", "h", "i",
                "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"};

        String randomkey = "";
        for (int i = 0; i < numdigit; i++) {

            int random = rand.nextInt(36);
            randomkey += arr[random];
        }

        if (prefix == null) {
            prefix = "RH";
        }

        return prefix + dateFormat + randomkey;
    }

    public void approvalby(Exchange exchange) {
        Message in = exchange.getIn();
        Message out = exchange.getOut();
        Exception except = exchange.getException();
//        String body = in.getBody(String.class);
        Integer httpResponseCode = in.getHeader(exchange.HTTP_RESPONSE_CODE, Integer.class);
        Gson gson = new Gson();
        Map<String, Object> identity = exchange.getProperty("identity", Map.class);

        Map<String, Object> getServiceResponse = new HashMap<String, Object>();
        Map<String, Object> status = new HashMap<String, Object>();
        Map<String, Object> rslt = new HashMap<String, Object>();
        List<Map<String, Object>> db = in.getBody(List.class);
        List<String> listby = new ArrayList<>();
        for (int i = 0 ; i < db.size(); i++){
            if (db.get(i).get("approvalby") != null){
                listby.add(db.get(i).get("approvalby").toString());
            }
        }
        Date date = new Date();
        status.put("responsecode", "0000");
        status.put("responsedesc", "Success");
        status.put("responsemessage", "Approvalby listed!");

        getServiceResponse.put("identity", identity);
        getServiceResponse.put("status", status);
        getServiceResponse.put("result", listby);
        String result = gson.toJson(getServiceResponse);
//        logger.info("HttpResponCode" + httpResponseCode);
//        logger.info("Responsemessage" + in.getHeader("error", String.class));
//        logger.info("result error" + result);
        out.setBody(result);
    }

    public void issuedby(Exchange exchange) {
        Message in = exchange.getIn();
        Message out = exchange.getOut();
        Exception except = exchange.getException();
//        String body = in.getBody(String.class);
        Integer httpResponseCode = in.getHeader(exchange.HTTP_RESPONSE_CODE, Integer.class);
        Gson gson = new Gson();
        Map<String, Object> identity = exchange.getProperty("identity", Map.class);

        Map<String, Object> getServiceResponse = new HashMap<String, Object>();
        Map<String, Object> status = new HashMap<String, Object>();
        Map<String, Object> rslt = new HashMap<String, Object>();
        List<Map<String, Object>> db = in.getBody(List.class);
        List<String> listby = new ArrayList<>();
        for (int i = 0 ; i < db.size(); i++){
            if (db.get(i).get("createdby") != null){
                listby.add(db.get(i).get("createdby").toString());
            }
        }
        Date date = new Date();
        status.put("responsecode", "0000");
        status.put("responsedesc", "Success");
        status.put("responsemessage", "Issuedby listed!");

        getServiceResponse.put("identity", identity);
        getServiceResponse.put("status", status);
        getServiceResponse.put("result", listby);
        String result = gson.toJson(getServiceResponse);
//        logger.info("HttpResponCode" + httpResponseCode);
//        logger.info("Responsemessage" + in.getHeader("error", String.class));
//        logger.info("result error" + result);
        out.setBody(result);
    }
//    public void resRejected(Exchange exchange) {
//        Message in = exchange.getIn();
//        Message out = exchange.getOut();
//        Gson gson = new Gson();
//        logger.info("cek :::");
//
//        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        Date d = new Date();
//        Date createddate;
//        createddate = d;
//        exchange.setProperty("dateend", sdf2.format(createddate));
//
//        logger.info("cek :::");
//        Map<String, Object> identity = exchange.getProperty("identity", Map.class);
//        Map<String, Object> getServiceResponse = new LinkedHashMap<>();
//        Map<String, Object> status = exchange.getProperty("status", Map.class);
//        Map<String, Object> result = exchange.getProperty("data", Map.class);
//
////        String reqhistoryid = GenerateId("RH", DateUtils.now(), 2);
//        Date approvaldate = DateUtils.now();
//        String createdby = identity.get("userid").toString();
////        exchange.setProperty("reqhistoryid", reqhistoryid);
//        exchange.setProperty("approvaldate", approvaldate);
//        exchange.setProperty("createdby", createdby);
//        status.put("responsecode", "0000");
//        status.put("responsedesc", "Success");
//
//        logger.info("cek :::");
//
//        result.put("approvalby", exchange.getProperty("approvalby"));
//        result.put("responsedata", exchange.getProperty("responsedata"));
////        logger.info("result nih :::" + result);
//
//        logger.info("cek :::");
//        getServiceResponse.put("status", status);
//        getServiceResponse.put("identity", identity);
//        getServiceResponse.put("result", result);
//        String response = gson.toJson(getServiceResponse);
//        out.setBody(response);
////        logger.info("RESPONSE :::" + response);
//        exchange.setProperty("resrjct", response);
//    }
//
//    public void resRevise(Exchange exchange) {
//        Message in = exchange.getIn();
//        Message out = exchange.getOut();
//        Gson gson = new Gson();
//        logger.info("cek :::");
//
//        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        Date d = new Date();
//        Date createddate;
//        createddate = d;
//        exchange.setProperty("dateend", sdf2.format(createddate));
//        logger.info("cek :::");
//
//        Map<String, Object> identity = exchange.getProperty("identity", Map.class);
//        Map<String, Object> getServiceResponse = new LinkedHashMap<>();
//        Map<String, Object> status = new HashMap<>();
//        Map<String, Object> result = new HashMap<>();
//
////        String reqhistoryid = GenerateId("RH", DateUtils.now(), 2);
//        Date approvaldate = DateUtils.now();
//        String createdby = identity.get("userid").toString();
////        exchange.setProperty("reqhistoryid", reqhistoryid);
//        exchange.setProperty("approvaldate", approvaldate);
//        exchange.setProperty("createdby", createdby);
//        status.put("responsecode", "0000");
//        status.put("responsedesc", "Success");
//        logger.info("cek :::");
//
//
//        result.put("approvalby", exchange.getProperty("approvalby"));
//        result.put("responsedata", exchange.getProperty("responsedata"));
////        logger.info("result nih :::" + result);
//
//        getServiceResponse.put("status", status);
//        getServiceResponse.put("identity", identity);
//        getServiceResponse.put("result", result);
//        String response = gson.toJson(getServiceResponse);
//        out.setBody(response);
////        logger.info("RESPONSE :::" + response);
//        exchange.setProperty("resrjct", response);
//        logger.info("cek :::");
//    }
}
