package id.co.asyst.amala.utils;

import com.google.gson.Gson;
import id.co.asyst.commons.core.utils.DateUtils;
import org.apache.camel.Exchange;
import org.apache.camel.Message;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Utils {

    public void getData(Exchange exchange) {
        Message in = exchange.getIn();
        Message out = exchange.getOut();
//        String memberid = exchange.getProperty("memberid").toString();
//        List<Map<String,Object>> body = in.getBody(List.class);
        Map<String, Object> body2 = in.getBody(Map.class);
        System.out.println(body2);
    }

    public void validation(Exchange exchange) {
        Boolean isvalid = true;
        Boolean usememberid = false;
        Map<String, Object> datarequest = exchange.getProperty("data",Map.class);
        if (datarequest.get("memberid") == null){
            System.out.println("memberid null");
            if (datarequest.get("email") == null){
                System.out.println("email null");
                isvalid = false;
                exchange.setProperty("rescode", "9005");
                exchange.setProperty("resdesc", "Invalid data!");
                exchange.setProperty("resmsg", "Memberid is required!");
            } else {
                System.out.println("email notnull");
                isvalid = true;
                exchange.setProperty("email", datarequest.get("email").toString());
            }
        } else {
            System.out.println("memberid notnull");
            usememberid = true;
            exchange.setProperty("memberid", datarequest.get("memberid").toString());
        }


        exchange.setProperty("isvalid", isvalid);
        exchange.setProperty("usememberid", usememberid);
    }

    public void generateRequestMember(Exchange exchange) {
        Message in = exchange.getIn();
        Message out = exchange.getOut();
        Gson gson = new Gson();
        Map<String, Object> identity = exchange.getProperty("identity", Map.class);
        Map<String, Object> requestawal = exchange.getProperty("data", Map.class);
        Map<String, Object> mapbodyreq = new LinkedHashMap<>();
        Map<String, Object> mapparameter = new LinkedHashMap<>();
        Map<String, Object> mapdata = new LinkedHashMap<>();

        mapdata.put("memberid", requestawal.get("memberid"));
        mapdata.put("type", "SUMMARY");
        System.out.println("mapdata" +mapdata);

        mapparameter.put("data", mapdata);
        System.out.println("mapparameter" +mapparameter);

        mapbodyreq.put("parameter", mapparameter);
        mapbodyreq.put("identity", identity);
        System.out.println("mapbodyreq" + mapbodyreq);
        String response = gson.toJson(mapbodyreq);
        System.out.println(response);
        out.setBody(response);
    }

    public void generateResult(Exchange exchange) {
        Message in = exchange.getIn();
        Message out = exchange.getOut();
        Gson gson = new Gson();

        Boolean resultava = true;
        Map<String, Object> identity = exchange.getProperty("identity", Map.class);
        Map<String, Object> datareq = exchange.getProperty("data",Map.class);
        Map<String, Object> response = in.getBody(Map.class);
        Map<String, Object> initresult = (Map<String, Object>) response.get("result");
        System.out.println("INIT RESULT :::" +initresult);

        List<Map<String, Object>> result = new ArrayList<>();

        List<Map<String, Object>> membercards = (List<Map<String, Object>>) initresult.get("membercards");
        System.out.println("MEMBERCARDS :::" +membercards);
        List<Map<String, Object>> membertiers = (List<Map<String, Object>>) initresult.get("membertiers");
        System.out.println("MEMBERTIERS :::" +membertiers);
        List<Map<String, Object>> memberaddress  = (List<Map<String, Object>>) initresult.get("memberaddress");
        System.out.println("MEMBERADDRESS :::" +memberaddress);
        List<Map<String, Object>> membercontacts  = (List<Map<String, Object>>) initresult.get("membercontacts");
        System.out.println("MEMBERCONTACT :::" +membercontacts);
        List<Map<String, Object>> memberhobbies  = (List<Map<String, Object>>) initresult.get("memberhobbies");
        System.out.println("MEMBERHOBBIES :::" +memberhobbies);

        String keterangan = datareq.get("keterangan").toString();
        System.out.println("KETERANGAN ::: " +keterangan);

        if (keterangan.equals("membercards")){
            result = membercards;
        } else if (keterangan.equals("membertiers")){
            result = membertiers;
        } else if (keterangan.equals("memberaddress")){
            result = memberaddress ;
        } else if (keterangan.equals("membercontacts")){
            result = membercontacts ;
        } else if (keterangan.equals("memberhobbies")){
            result = memberhobbies ;
        }

        System.out.println("RESULT :::" +result);

        if (result.isEmpty()){
            resultava = false;
        }

        exchange.setProperty("resultfix", result);
        exchange.setProperty("resultava", resultava);
        exchange.setProperty("keterangan", keterangan);
    }
}
